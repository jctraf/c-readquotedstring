## Reading Quote-Delimited Strings

Example of reading a file that contains quote-delimited strings.

The program reads data from a file called: `airports.txt`  and outputs the data to the screen.

The `airports.txt` file contains information about the busiest airports in the world.  All strings are encapsulted by double quotes `"`.

### Useful functions
---

See the `getQuotedString(...)` function.

This function takes two parameters:

1. A pointer to an input file
2. A character array

The function reads a quote delimited string from the input file and saves the result to the character array specified in the parameter.

See code for usage examples and comments.


### Expected output from the program:
---

```
-------------------------------------------
What are the busiest airports in the world?
-------------------------------------------
Airport Name: Atlanta International Airport
Airport Code: ATL
Passenger Traffic: 54338000

Airport Name: Beijing Capital International Airport
Airport Code: PEK
Passenger Traffic: 49242000

... 
```
