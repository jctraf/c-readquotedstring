#include <stdio.h>
#include <string.h>

void getQuotedString(FILE * file, char *word);

int main() {

    FILE * inputFile = fopen("airports.txt","r");
    if (inputFile == NULL) {
        printf("Cannot open file.\n");
        return -1;
    }
    else {
        printf("Opened file successfully.\n");
    }

    printf("-------------------------------------------\n");
    printf("What are the busiest airports in the world?\n");
    printf("-------------------------------------------\n");

    // NOTE: This code assumes that the first "thing" in the file
    // is a quote-delimited string.

    char airportName[50] = "";
    getQuotedString(inputFile, airportName);

    // if the word in the file != "END", then continue reading the file
    while (strcmp(airportName, "END") != 0) {
        char airportCode[4] = "";
        getQuotedString(inputFile, airportCode);

        float passengerTraffic = 0;
        fscanf(inputFile, "%f", &passengerTraffic);

        // print everything to the screen
        printf("Airport Name: %s\n", airportName);
        printf("Airport Code: %s\n", airportCode);
        printf("Passenger Traffic: %.0f\n", passengerTraffic);
        printf("\n");

        // read next item
        getQuotedString(inputFile, airportName);
    }

    return 0;
}


void getQuotedString(FILE * file, char word[]) {

    // clears the word[] array before using it
    //memset(word, 0, sizeof word);

    // --------------------
    // 1. Find the first occurence of a " symbol
    // --------------------
    char letter;
    fscanf(file, "%c", &letter);

    // Code to skip any leading whitespace before the string
    while (letter == ' ' || letter == '\n') {
        fscanf(file, "%c", &letter);
    }


    // --------------------
    // 2. You've found a " symbol!
    // This means that we are the beginning of a quote-delimited string.
    // Thus, you must manually construct a string, one letter at a time
    // The final string will be stored in the word[] array.
    // --------------------
    if (letter == '"') {
        // keeps track of what position we are in the string
        int pos = 0;

        // Get the next character from the file
        // REASON: Remember, we do NOT want to add the initial " to the string
        // We want everything that comes after the "
        fscanf(file, "%c", &letter);

        // loop through the file until you find the other " symbol
        while (letter != '"') {
            // add the character to the word[] array (word = final string)
            word[pos] = letter;

            // increment the position in the word[] array
            pos++;

            // read next character from file
            fscanf(file, "%c", &letter);
        }
        // manually add on the \0 at the end of a string
        word[pos] = '\0';

        // @DEBUG: print out the final word to the screen
        // printf("%s\n", word);
    }
    else {
        // The first character after all the white spaces was NOT a "
        // (Maybe it was a number? Maybe it was a string WIHOUT "?)
        // You don't know and it doesn't matter. Just skip and move on.
        printf("No quote-delimited string found.\n");
    }
}
